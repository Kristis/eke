$(document).ready(function(){
var chart = new Chartist.Line('.ct-chart', {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  series: [
    [12, 4, 2, 8, 5, 4, 6, 2, 3, 3, 4, 6],
    [4, 8, 9, 3, 7, 2, 10, 5, 8, 1, 7, 10]
  ]
}, {
  low: 0,
  showLine: false,
  axisX: {
    showLabel: false,
    offset: 0
  },
  axisY: {
    showLabel: false,
    offset: 0
  }
});
var seq = 0;
chart.on('created', function() {
  seq = 0;
});
chart.on('draw', function(data) {
  if(data.type === 'point') {
    data.element.animate({
      opacity: {
        begin: seq++ * 80,
        dur: 500,
        from: 0,
        to: 1
      },
      x1: {
        begin: seq++ * 80,
        dur: 500,
        from: data.x - 100,
        to: data.x,
        easing: Chartist.Svg.Easing.easeOutQuart
      }
    });
  }
});
chart.on('created', function() {
  if(window.__anim0987432598723) {
    clearTimeout(window.__anim0987432598723);
    window.__anim0987432598723 = null;
  }
  window.__anim0987432598723 = setTimeout(chart.update.bind(chart), 8000);
});
new Chartist.Line('.ct-chart2', {
		labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri','Sat','Sun'],
  series: [
    [5, 4, 3, 4, 3, 8, 3]
  ],
    fullWidth: true,
	});
 new Chartist.Line('.ct-chart3', {
		labels: ['Desimties metu','Dvidesimties metu','Trisdesimties metu','Keturiasdesimties metu'],
		series: [[100, 120, 250, 500]]
	});
	var chart = new Chartist.Line('.ct-chart4', {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
  series: [{
    name: 'series-1',
    data: [5, 2, -4, 2, 0, -2, 5, -3]
  }, {
    name: 'series-2',
    data: [4, 3, 5, 3, 1, 3, 6, 4]
  }, {
    name: 'series-3',
    data: [2, 4, 3, 1, 4, 5, 3, 2]
  }]
}, {
  fullWidth: true,
  series: {
    'series-1': {
      lineSmooth: Chartist.Interpolation.step()
    },
    'series-2': {
      lineSmooth: Chartist.Interpolation.simple(),
      showArea: true
    },
    'series-3': {
      showPoint: false
    }
  }
}, [
  ['screen and (max-width: 320px)', {
    series: {
      'series-1': {
        lineSmooth: Chartist.Interpolation.none()
      },
      'series-2': {
        lineSmooth: Chartist.Interpolation.none(),
        showArea: false
      },
      'series-3': {
        lineSmooth: Chartist.Interpolation.none(),
        showPoint: true
      }
    }
  }]
]);
		new Chartist.Pie('.ct-chart5', {
		series: [2, 3, 4, 5]
		}, {
			donut: true,
			donutWidth: 100,
			donutSolid: true,
			startAngle: 200,
			showLabel: true
		});
	chart.on('draw', function(data) {
		if(data.type === 'slice') {
		var pathLength = data.element._node.getTotalLength();
		data.element.attr({
		'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
		});
    var animationDefinition = {
		'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        fill: 'freeze'
		}
    };
    if(data.index !== 0) {
		animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
		}	
    data.element.attr({
		'stroke-dashoffset': -pathLength + 'px'
		});
    data.element.animate(animationDefinition, false);
	}
	});
	chart.on('created', function() {
		if(window.__anim21278907124) {
		clearTimeout(window.__anim21278907124);
		window.__anim21278907124 = null;
		}
	window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
	});
  });