$(document).ready(function(){	

	/////////////Pirmas///////////////
  
	// Create a simple bi-polar bar chart
	var chart = new Chartist.Bar('#chart01', {
		labels: ['D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10'],
		series: [
		[-8, -6, -4, -2, 0, 2, 4, 6, 8, 9]
	]
	}, {
		high: 10,
		low: -10,
		axisX: {
		labelInterpolationFnc: function(value, index) {
		return index % 2 === 0 ? value : null;
		}
	}
	});

		// Listen for draw events on the bar chart
	chart.on('draw', function(data) {
		// If this draw event is of type bar we can use the data to create additional content
		if(data.type === 'bar') {
		// We use the group element of the current series to append a simple circle with the bar peek coordinates and a circle radius that is depending on the value
		data.group.append(new Chartist.Svg('circle', {
			cx: data.x2,
			cy: data.y2,
			r: Math.abs(Chartist.getMultiValue(data.value)) * 2 + 5
			}, 'ct-slice-pie'));
		}
	});
	
	////////////////Antras//////////////////////////
	
	new Chartist.Line('#chart1', {
		labels: ['Desimties metu','Dvidesimties metu','Trisdesimties metu','Keturiasdesimties metu'],
		series: [[100, 120, 250, 500]]
	});

	////////////////Trecias//////////////////////////////	
	
	var chart = new Chartist.Line('#chart2', {
		labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
		series: [
		[1, 5, 2, 5, 4, 3],
		[2, 3, 4, 8, 9, 12],
		[5, 4, 3, 2, 8, 10]
		]
	}, {
		low: 0,
		showArea: true,
		showPoint: false,
		fullWidth: true
	});

	chart.on('draw', function(data) {
		if(data.type === 'line' || data.type === 'area') {
			data.element.animate({
			d: {
			begin: 2000 * data.index,
			dur: 2000,
        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
			to: data.path.clone().stringify(),
			easing: Chartist.Svg.Easing.easeOutQuint
			}
		});
		}
	});

		// Listen for draw events on the bar chart
	chart.on('draw', function(data) {
		// If this draw event is of type bar we can use the data to create additional content
		if(data.type === 'bar') {
		// We use the group element of the current series to append a simple circle with the bar peek coordinates and a circle radius that is depending on the value
		data.group.append(new Chartist.Svg('circle', {
			cx: data.x2,
			cy: data.y2,
			r: Math.abs(Chartist.getMultiValue(data.value)) * 2 + 5
			}, 'ct-slice-pie'));
		}
	});
		
	///////////////Ketvirtas//////////////////////////////
	
	var chart = new Chartist.Pie('#chart3', {
		series: [2, 2, 2, 2, 2],
		labels: [1, 2, 3, 4, 5]
		}, {
		donut: true,
		showLabel: false
		});

	chart.on('draw', function(data) {
		if(data.type === 'slice') {
		// Get the total path length in order to use for dash array animation
		var pathLength = data.element._node.getTotalLength();

		// Set a dasharray that matches the path length as prerequisite to animate dashoffset
		data.element.attr({
		'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
		});

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
		'stroke-dashoffset': {
			id: 'anim' + data.index,
			dur: 1000,
        from: -pathLength + 'px',
			to:  '0px',
			easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
			fill: 'freeze'
		}
    }	;

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
		if(data.index !== 0) {
		animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
		}
		// We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
		'stroke-dashoffset': -pathLength + 'px'
		});

		// We can't use guided mode as the animations need to rely on setting begin manually
		// See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
	}
	});

	// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
	chart.on('created', function() {
		if(window.__anim21278907124) {
		clearTimeout(window.__anim21278907124);
		window.__anim21278907124 = null;
		}	
		window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
	});

	///////////////////Penktas/////////////////////////////////////////////////

	new Chartist.Pie('#chart4', {
		series: [2, 3, 4, 5]
		}, {
			donut: true,
			donutWidth: 100,
			donutSolid: true,
			startAngle: 200,
			showLabel: true
		});

	chart.on('draw', function(data) {
		if(data.type === 'slice') {
		// Get the total path length in order to use for dash array animation
		var pathLength = data.element._node.getTotalLength();

		// Set a dasharray that matches the path length as prerequisite to animate dashoffset
		data.element.attr({
		'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
		});

		// Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
		'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
		}
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
		animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
		}	
    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
		'stroke-dashoffset': -pathLength + 'px'
		});

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
	}
	});

	// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
	chart.on('created', function() {
		if(window.__anim21278907124) {
		clearTimeout(window.__anim21278907124);
		window.__anim21278907124 = null;
		}
	window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
	});

	//////////Pabaiga//////////////

});

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	